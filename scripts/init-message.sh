#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"
source "$TMUX_SHARE_DIR/functions.sh"

user=
clientName="$(tmux display-message -p '#{client_name}')"
if [ "$clientName" == "$TMUX_SHARE_OWNER_CLIENT" ]; then
	user="$TMUX_SHARE_OWNER"
elif [ "$clientName" == "$TMUX_SHARE_GUEST_CLIENT" ]; then
	user="$TMUX_SHARE_GUEST"
else
	exit 0
fi

tmux command-prompt -t "$clientName" -p "<$user> " "run '$TMUX_SHARE_DIR/scripts/send-message.sh $user "'"'"%%%"'"'"'"

