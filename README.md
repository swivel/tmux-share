# tmux-share

`tmux-share` allows users to share a TMUX session with others. Think of it
like 'screensharing' your terminal!

## Requirements

| :warning: `tmux-share` requires the following to run on the host machine: |
| :-- |

1. Bash
2. wget
3. TMUX
4. [ngrok](https://ngrok.com/) (must be accessible from your PATH)
5. sshd (OpenSSH)


## Installation

Install using [tpm](https://github.com/tmux-plugins/tpm#tmux-plugin-manager)
by adding the following to your tmux config:

```
set -g @plugin 'git@gitlab.com/swivel/tmux-share'
```

This plugin works by temporarily giving a trusted remote user access to share
your TMUX session via SSH. Because of this, it uses the
`~/.ssh/authorized_keys2` to setup their Public SSH Keys. You may want to
configure this to use a different file.

To set a different key file, add the following to your `.tmux.conf`:

```
set -g @tmuxshareKeyFile '/home/myuser/.ssh/authorized_keys'
```

By default, it uses `.ssh/authorized_keys2`. However, this file has been
deprecated for many years. If it doesn't work, you can attempt to use your
`.ssh/authorized_keys` file. This script will attempt to non-destructively
add their Public SSH Keys, and remove them when the session is correctly
stopped.

## Usage

> **SECURITY WARNING** 
>
> This plugin is the equivalent of handing your laptop to a colleague.
> They'll have access to everything that you currently do in your active
> terminal session.
>
> **_Only give access to someone you absolutely trust._** As is clear in the
> license, this software provides no warranties, and I am not responsible if
> someone damages your machine. This software is intended to be used between
> trusted parties, not some random person on IRC that said they'd help you.
> **_Use at your own risk._**


### Start Sharing

> **Notice**: 
> Before initiating a sharing session, make sure your colleague has a Github
> account with up-to-date public SSH keys.

To start sharing, press <kbd>prefix</kbd>+<kbd>Shift</kbd>+<kbd>h</kbd> (for "Help Me!", the
original name for this utility). Then, enter the Github username of your
trusted colleague.


### Stop Sharing

> **Notice**:
> This forcibly disconnects the end user and revokes SSH.
> At least say goodbye first.

To stop sharing, press <kbd>prefix</kbd>+<kbd>Shift</kbd>+<kbd>h</kbd> again.


### Send / Read messages

You can send and receive messages too! It's a bit rudamentary, but it works for now.

To send a message, press <kbd>prefix</kbd>+<kbd>Shift</kbd>+<kbd>t</kbd> and type in your message.

To read all the messages that have been sent and received, press <kbd>prefix</kbd>+<kbd>Shift</kbd>+<kbd>\`</kbd>

