#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"

init_sharing () {
	source "$TMUX_SHARE_DIR/scripts/pre-sharing.sh"
}

end_sharing () {
	source "$TMUX_SHARE_DIR/scripts/stop-sharing.sh"
}

if [ -z "$TMUX_SHARE_ID" ]; then
	init_sharing
else
	end_sharing
fi

