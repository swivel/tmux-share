#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"
source "$TMUX_SHARE_DIR/functions.sh"

if [ -z "$1" ]; then
	tmux run-shell "$TMUX_SHARE_DIR/scripts/pre-sharing.sh"
	exit 0
else
	vset TMUX_SHARE_GUEST $1
fi

setup_ca () {
	if [ -z "$TMUX_SHARE_KEY_FILE" ]; then
		vset TMUX_SHARE_KEY_FILE "$HOME/.ssh/authorized_keys"
	fi

	if [ -z "$TMUX_SHARE_CA_FILE" ]; then
		vset TMUX_SHARE_CA_FILE "$(dirname $TMUX_SHARE_KEY_FILE)/tmux-share-ca"
	fi

	if [ ! -z $TMUX_SHARE_CA_FILE ]; then
		tell "Generating TMUX Share Certificate Authority"
		ssh-keygen -f "$TMUX_SHARE_CA_FILE" -N '' 2>&1 > /dev/null || {
			fatal "Unable to generate TMUX SHARE Certificate Authority"
		}
	fi
}

setup_ca
