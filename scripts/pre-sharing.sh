#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"

ask_github_un () {
	prompt="TMUX Share: Who would you like to share with? Github Username:"
	tmux command-prompt -p "$prompt" "run-shell '$TMUX_SHARE_DIR/scripts/start-sharing.sh %1'"
}

ask_github_un

