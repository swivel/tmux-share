#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"
source "$TMUX_SHARE_DIR/functions.sh"

if [ -z "$TMUX_SHARE_ID" ]; then
	fatal "No tmux-share session running."
fi

started=false
found=false
ngrok tcp 22 -log=stdout | while read line; do
	echo $line
	[ "$started" = "false" ] && { started=true; tell_owner "NGROK Started..."; }
	[ "$found" = "false" ] && {
		rawinfo=$(echo $line | grep 'tcp:' |  sed -E 's,^.+tcp://([^:]+)\:([0-9]+)$,\1 \2,')
		if [ ! -z "$rawinfo" ]; then
			read -ra info <<< "$rawinfo"
			vset TMUX_SHARE_NGROK_HOST "${info[0]}"
			vset TMUX_SHARE_NGROK_PORT "${info[1]}"
			prompt="TMUX Share: Join command >> ssh $TMUX_SHARE_OWNER@${info[0]} -p ${info[1]} -t 'tmux attach-session -t $TMUX_SHARE_GUEST' << (press <Enter> to close this message)"
			tell_owner $prompt # That way it's stored in 'show-messages'
			tmux confirm-before -p "$prompt" -t $TMUX_SHARE_OWNER_CLIENT "exit 0"
			found=true
		fi
	}
done

