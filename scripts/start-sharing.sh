#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"
source "$TMUX_SHARE_DIR/functions.sh"

if [ -z "$1" ]; then
	tmux run-shell "$TMUX_SHARE_DIR/scripts/pre-sharing.sh"
	exit 0
else
	vset TMUX_SHARE_GUEST $1
fi

get_gh_keys () {
	if [ -z "$TMUX_SHARE_KEY_FILE" ]; then
		if [ -z "$(tmux show-option -g -v @tmuxshareKeyFile)" ]; then
			vset TMUX_SHARE_KEY_FILE "$(tmux show-option -g -v @tmuxshareKeyFile)"
		else
			vset TMUX_SHARE_KEY_FILE "$HOME/.ssh/authorized_keys2"
		fi
	fi

	if [ ! -f $TMUX_SHARE_KEY_FILE ]; then
		touch $TMUX_SHARE_KEY_FILE
		tmux display-message "Creating $TMUX_SHARE_KEY_FILE"
	fi

	tmpkeys=$(mktemp)

	wget https://github.com/$TMUX_SHARE_GUEST.keys -O $tmpkeys || {
		fatal "ERROR retrieving key files for Github User '$TMUX_SHARE_GUEST'. Please try again."
	}

	backup=$(mktemp authorized_keys.XXXXX)
	chmod 600 $backup
	cat $TMUX_SHARE_KEY_FILE > $backup
	vset TMUX_SHARE_KEY_FILE_BACKUP $backup

	content="$(getkeywrap_start)\n$(cat $tmpkeys)\n$(getkeywrap_end)\n$([ -f $TMUX_SHARE_KEY_FILE ] && cat $TMUX_SHARE_KEY_FILE)"
	echo -e "$content" > $TMUX_SHARE_KEY_FILE || {
		fatal "Unable to install $TMUX_SHARE_GUEST's SSH keys into $TMUX_SHARE_KEY_FILE"
	}

	tell_owner "Successfully authorized $TMUX_SHARE_GUEST"
}

gen_id () {
	if [ ! -z "$TMUX_SHARE_ID" ]; then
		fatal "ERROR: tmux-share session already started"
	fi

	id=$(date +%s%N)

	vset TMUX_SHARE_ID $id
}

set_owner () {
	username=$USER
	if [ -z "$username" ]; then
		username=$(whoami)
	fi

	if [ -z "$username" ]; then
		fatal "ERROR: tmux-share unable to determine user based"
	fi

	vset TMUX_SHARE_OWNER "$username"

	sessionName=$(tmux display-message -p '#{client_session}')
	vset TMUX_SHARE_OWNER_SESSION "$sessionName"

	clientName=$(tmux display-message -p '#{client_name}')
	vset TMUX_SHARE_OWNER_CLIENT "$clientName"
}

start_sharing () {
	tell_owner "Starting Sharing Session..."

	procWindow="tmuxshare-$TMUX_SHARE_ID"
	tmux new-session -d -s "$procWindow" -n 'ngrok' "$TMUX_SHARE_DIR/scripts/start-ngrok.sh" || {
		fatal "ERROR: tmux-share could not start ngrok"
	}

	tmux new-session -d -s $TMUX_SHARE_GUEST -t $TMUX_SHARE_OWNER_SESSION
	tmux set-hook -t $TMUX_SHARE_GUEST client-attached "run-shell '$TMUX_SHARE_DIR/scripts/init-guest.sh'"

	vset TMUX_SHARE_PROC_WINDOW $procWindow
	vset TMUX_SHARE_PROC_NGROK "$procWindow:ngrok"

	# tmux capture-pane -J -S - -t "$procWindow:ngrok" -p
}

gen_id
set_owner
get_gh_keys || clean_vars
start_sharing

