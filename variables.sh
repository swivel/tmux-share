#!/usr/bin/env bash

vunset_all () {
	vunset TMUX_SHARE_ID
	# vunset TMUX_SHARE_DIR
	vunset TMUX_SHARE_OWNER
	vunset TMUX_SHARE_OWNER_SESSION
	vunset TMUX_SHARE_GUEST
	vunset TMUX_SHARE_GUEST_SESSION
	# vunset TMUX_SHARE_KEY_FILE
	vunset TMUX_SHARE_NGROK_HOST
	vunset TMUX_SHARE_NGROK_PORT
	vunset TMUX_SHARE_PROC_WINDOW
	vunset TMUX_SHARE_PROC_NGROK
}

vset () {
	tmux set-environment -g $1 "$2"
	export $1="$2"
}

vunset () {
	tmux set-environment -g -r $1
	unset $1
}

vsetl () {
	tmux set-environment $1 $2
}

vunsetl () {
	tmux set-environment -r $1
	unset $1
}

getkeywrap_start () {
	echo "# @tmux-share $TMUX_SHARE_ID start"
	echo "# GENERATED DO NOT EDIT"
}

getkeywrap_end () {
	echo "# GENERATED DO NOT EDIT"
	echo "# @tmux-share $TMUX_SHARE_ID end"
}

CALLER_REL=$(caller | cut -d ' ' -f 2)
CALLER_NAME=$(basename "$CALLER_REL")
CALLER_DIR="$(cd "$( dirname "$CALLER_REL" )" && pwd)"

if [ -z "$TMUX_SHARE_DIR" ] && [ "$CALLER_NAME" == "tmux-share.tmux" ]; then
	vset TMUX_SHARE_DIR $CALLER_DIR
fi

