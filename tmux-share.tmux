#!/usr/bin/env bash

CURRENT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
source "$CURRENT_DIR/variables.sh"
source "$CURRENT_DIR/functions.sh"

init () {
	if [ -z "$TMUX_SHARE_ID" ]; then
		vunset_all #variables.sh
		clean_keys #functions.sh
	else
		source "$CURRENT_DIR/scripts/init-guest.sh"
	fi
}

source "$CURRENT_DIR/bindings.sh"

init
