#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"

msgPrefix="TMUX Share:"

tell () {
	tmux display-message "$msgPrefix $*"
}

tell_owner () {
	owner=$TMUX_SHARE_OWNER_CLIENT
	[ ! -z "$owner" ] && tmux display-message -c $owner "$msgPrefix $*"
}

tell_guest () {
	guest=$TMUX_SHARE_GUEST_CLIENT
	[ ! -z "$guest" ] && tmux display-message -c $guest "$msgPrefix $*"
}

throw () {
	tell "ERROR: $*"
}

fatal () {
	throw $*
	exit 1
}

depcheck () {
	command -v ngrok >/dev/null 2>&1 || {
		fatal "This script requires ngrok. Please install it into your PATH before continuing."
	}
	
	command -v wget >/dev/null 2>&1 || {
		fatal "This script requires wget. Please install it into your PATH before continuing."
	}
}

clean_keys () {
	if [ ! -z "$TMUX_SHARE_KEY_FILE" ]; then
		if [ -f $TMUX_SHARE_KEY_FILE ]; then
			lines=($(grep -n '# @tmux-share' $TMUX_SHARE_KEY_FILE | cut -f1 -d:))
			if [ ! -z "${lines[0]}" ] && [ ! -z "${lines[1]}" ]; then
				tmp=$(mktemp)
				trap "rm -f $tmp" EXIT
				chmod 600 $tmp
				cat $TMUX_SHARE_KEY_FILE > $tmp
				sed -i'' -e "${lines[0]},${lines[1]}d" $TMUX_SHARE_KEY_FILE || {
					cat $tmp > $TMUX_SHARE_KEY_FILE
				}
			fi
		fi
	fi
}

depcheck

