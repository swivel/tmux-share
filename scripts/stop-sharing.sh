#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }
source "$TMUX_SHARE_DIR/variables.sh"
source "$TMUX_SHARE_DIR/functions.sh"

[ -z "$TMUX_SHARE_ID" ] && exit 0

stop_sharing () {
	tell_owner "Stopping NGROK..."
	tmux kill-window -t $TMUX_SHARE_PROC_WINDOW
	tell_owner "Killing $TMUX_SHARE_GUEST's session..."
	tmux kill-session -t $TMUX_SHARE_GUEST
	tell_owner "Cleaning keys and unsetting share environment..."
	clean_keys && vunset_all
	tell_owner "Done! Thanks for sharing."
}

stop_sharing

