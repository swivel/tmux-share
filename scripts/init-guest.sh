#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }

source "$TMUX_SHARE_DIR/variables.sh"
source "$TMUX_SHARE_DIR/functions.sh"

init_guest () {
	[ -z "$TMUX_SHARE_ID" ] && exit 0
	[ -z "$TMUX_SHARE_GUEST" ] && exit 0

	sessionName="$(tmux display-message -p '#{client_session}')"
	[ "$TMUX_SHARE_GUEST" != "$sessionName" ] && exit 0

	vset TMUX_SHARE_GUEST_SESSION "$sessionName"

	clientName=$(tmux display-message -p '#{client_name}')
	vset TMUX_SHARE_GUEST_CLIENT "$clientName"

	tell_owner "<$TMUX_SHARE_GUEST> just connected! Press <prefix>T to message them. Press <prefix>~ to see messages they have sent you."
	tell_guest "Welcome $TMUX_SHARE_GUEST! Press <prefix>T to talk to <$TMUX_SHARE_OWNER>. Press <prefix>~ to see messages they have sent you."
}

init_guest

