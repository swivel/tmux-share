#!/usr/bin/env bash
eval "$(tmux show-environment -g -s | grep TMUX_SHARE_)"
[ -z "$TMUX_SHARE_DIR" ] && { echo "ERROR: tmux-share wasn't properly initialized"; exit 1; }

tmux bind-key -T prefix H run-shell "$TMUX_SHARE_DIR/scripts/toggle-sharing.sh"
tmux bind-key -T prefix T run-shell "$TMUX_SHARE_DIR/scripts/init-message.sh"

